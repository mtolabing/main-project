import java.io.*;
import java.util.*;

/**
 * Created by jt186048 on 10/28/2015.
 */
public class TargetFilesUtil {

    public static final String HEADER_COLUMN = "user.email";
    public static final String EOL_CHAR = "\u0004";

    /**
     * Checks an array for duplicate values
     *
     * @param values
     * @return number of duplicates
     */
    public static DuplicateResult countDuplicates(List<String> values) {

        DuplicateResult duplicateResult = new DuplicateResult();
        Set set = new HashSet<String>();
        for (String val : values) {
            if (set.add(val) == false) {
                duplicateResult.increment();
                duplicateResult.addValue(val);
            }
        }
        return duplicateResult;
    }

    /**
     * Reads values from the relevant column in a file
     *
     * @param lines
     * @param indices
     * @return
     */
    public static List<String> getRelevantColumnValues(List<String> lines,
                                                       List<Integer> indices, String delimiter) {
        List<String> relevantColumn = new ArrayList<String>();

        if (lines.size() > 0) {
            if (delimiter.equals("t") && !lines.get(0).contains("\t")) return null;
            else if (!delimiter.equals("t") && !lines.get(0).contains(delimiter)) return null;
        }
        for (String line : lines) {
            String[] lineContent = line.split("\\" + delimiter);
            String combinedVals = "";
            for (int index : indices) {
                if (lineContent.length > 1)
                    combinedVals = combinedVals + lineContent[index];
            }
            relevantColumn.add(combinedVals);
        }

        return relevantColumn;
    }

    public static List<String> subtractHeaderFooter(List<String> lines, String header) {

        if (lines.size() < 1) return lines;

        String useHeader = (header == null || header.equals("")) ? HEADER_COLUMN : header;
        if (lines.get(0).toLowerCase().contains(useHeader.toLowerCase())) {
            lines.remove(0);
        }

        if (lines.size() > 1 && lines.get(lines.size() - 1).contains(EOL_CHAR)) {
            lines.remove(lines.size() - 1);
        }

        return lines;

    }

    public static String generateResultsFile(File resultsFile, List<UtilResult> results) {

        if (!resultsFile.getName().contains(".")) {
            resultsFile = new File(resultsFile.getAbsolutePath() + ".csv");
        }
        try {
            FileWriter writer = new FileWriter(resultsFile);
            writer.append("File,Count,Duplicates,Remarks,Last Modified\n");
            for (UtilResult result : results) {
                writer.append(result.getFilename() + ",");
                writer.append(Integer.toString(result.getLines()) + ",");

                if (result.getDuplicates().getCount() != -1) {
                    writer.append(Integer.toString(result.getDuplicates().getCount()) + ",");
                    writer.append(result.getDuplicates().getValuesString() + ",");
                } else {
                    writer.append("INCORRECT DELIMITER FOR FILE ,");
                }
                writer.append(result.getDateModified() + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resultsFile.getAbsolutePath();
    }

    public static List<UtilResult> processFiles(File[] selectedFiles, String delimiter,
                                                String[] ctrlColIndicesString, String header) {
        List<Integer> ctrlColIndices = new ArrayList<Integer>();
        List<UtilResult> results = new ArrayList<UtilResult>();

        Arrays.sort(selectedFiles, new Comparator() {
            public int compare(Object o1, Object o2) {

                if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                    return +1;
                } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                    return -1;
                } else {
                    return 0;
                }
            }

        });

        for (String ctrlColIndexString : ctrlColIndicesString) {
            ctrlColIndexString = ctrlColIndexString.trim();
            if (!ctrlColIndexString.equals(""))
                ctrlColIndices.add(Integer.parseInt(ctrlColIndexString) - 1);
        }

        if (selectedFiles.length < 1) {
            return null;
        }

        for (File file : selectedFiles) {
            List<String> lines = new ArrayList<String>();
            try {
                if (file.exists()) {
                    BufferedReader reader = new BufferedReader(new FileReader(file));

                    String line = reader.readLine();
                    while (line != null) {
                        lines.add(line);
                        line = reader.readLine();
                    }
                }

                lines = TargetFilesUtil.subtractHeaderFooter(lines, header);
                List<String> ctrCol = TargetFilesUtil.getRelevantColumnValues(lines, ctrlColIndices, delimiter);
                DuplicateResult dupCounts = new DuplicateResult();
                if (ctrCol != null){
                    dupCounts = TargetFilesUtil.countDuplicates(ctrCol);
                }
                String dateModified = new Date(file.lastModified()).toString();

                UtilResult utilResult = new UtilResult(file.getName(), dateModified, lines.size(), dupCounts);
                results.add(utilResult);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return results;
    }


}
