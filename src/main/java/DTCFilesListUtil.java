import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by jt186048 on 3/7/2016.
 */
public class DTCFilesListUtil {

    public static int CATEGORY_DMC = 0;
    public static int CATEGORY_SGI = 1;
    public static int CATEGORY_SHARPS = 2;

    public static String[] RESOURCE_FILES = {"/preset-files/dmc-list.txt", "/preset-files/sgi-list.txt", "/preset-files/sharps-list.txt"};
    public static String introMessage = "Hi All,<br><br>The following production files have been generated " +
            "and are now available in Itrica.<br>";

    private int fileCount;

    public DTCFilesListUtil(){
        fileCount = 0;
    }

    public List<String> getCategoryFilenames(int category, List<String> fileNames) {
        List<String> categoryFiles = new ArrayList<String>();

        List<String> patternStrings = new ArrayList<String>();
        try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(RESOURCE_FILES[category])));

                String line = reader.readLine();
                while (line != null) {
                    patternStrings.add(line);
                    line = reader.readLine();
                }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String patternString : patternStrings) {
            boolean flag = false;
            for (String dirFile : fileNames) {
                if (Pattern.matches(patternString, dirFile)){
                    categoryFiles.add(dirFile);
                    flag = true;
                }
            }
            if (flag==false){
                System.out.println("No file found for: " +
                        RESOURCE_FILES[category] + " " + patternString);
            }
        }
        fileCount += categoryFiles.size();
        return categoryFiles;
    }

    public List<String> getFilenamesFromDir(String directory) {
        List<String> filenames = new ArrayList<String>();

        Calendar calendar = Calendar.getInstance(); // present time
        calendar.add(Calendar.HOUR_OF_DAY, -12);

        File dir = new File(directory);
        File[] files = dir.listFiles();

        if (files == null) return null;
        for (int i = 0; i < files.length; i++) {
            if (files[i].lastModified() > calendar.getTimeInMillis()) {
                filenames.add(files[i].getAbsoluteFile().getName());
            }
        }

        return filenames;
    }

    public String createMessage(String directory) {
        fileCount = 0;
        String message = "";

        // Get current date
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");

        List<String> filenames = getFilenamesFromDir(directory);

        // Get filenames from output directory
        message = introMessage;

        message = message + "<br><b>DMC Production Files for " + sdf.format(date) + "</b><br>";
        message = message + getCategoryMessage(CATEGORY_DMC,filenames);


        message = message + "<br><b>SHARPS Production Files for " + sdf.format(date) + "</b><br>";
        message = message + getCategoryMessage(CATEGORY_SHARPS,filenames);

        message = message + "<br><b>SGI Production Files for " + sdf.format(date) + "</b><br>";
        message = message + getCategoryMessage(CATEGORY_SGI,filenames);

        message = message + "<br><br> Total [" + fileCount + "] files posted.";

        return message;
    }

    public String getCategoryMessage(int category, List<String> filenames){
        String categoryMessage = "";

        if (filenames != null) {
            List<String> categoryFiles = getCategoryFilenames(category,filenames);
            if (categoryFiles.size() == 0) return "-none-<br>";
            for (String dmcFile : categoryFiles) {
                categoryMessage = categoryMessage + dmcFile + "<br>";
            }
        }

        return categoryMessage;
    }

    public File[] getFilesFromFilenames(String directory, List<String> filenames){
        List<File> fileList = new ArrayList<File>();
        File[] files = {};

        for (String filename : filenames){
            File file = new File(directory + "\\" + filename);
            fileList.add(file);
        }
        files = fileList.toArray(files);

        return files;
    }
}
