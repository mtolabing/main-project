import java.util.HashSet;
import java.util.Set;

/**
 * Created by jt186048 on 1/28/2016.
 */
public class DuplicateResult {

    private int count;
    private Set<String> values;

    public DuplicateResult(){
        count = 0;
        values = new HashSet<String>();
    }

    public int getCount() {
        return count;
    }

    public void increment(){
        this.count++;
    }

    public void addValue(String value){
        this.values.add(value);
    }

    public String getValuesString(){
        String valuesString = "";
        for(String value : values){
            valuesString = valuesString + value + "|";
        }
        return valuesString;
    }

}
