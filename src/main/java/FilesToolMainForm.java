import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jt186048 on 11/11/2015.
 */
public class FilesToolMainForm extends JFrame {
    private JTextField directoryField;
    private JComboBox delimiterComboBox;
    private JTextField colsDupCheck;
    private JButton selectFilesButton;
    private JList selectedFilesDisplayList;
    private JButton analyzeButton;
    private JButton cancelButton;
    private JPanel mainPanel;
    private JPanel directoryPanel;
    private JPanel delimiterPanel;
    private JPanel buttonsPanel;
    private JPanel listPanel;
    private JTextField headerIdField;
    private JPanel headerDetailsPanel;
    private JButton saveToButton;
    private JLabel saveToLabel;
    private JButton filesGenButton;
    private JComboBox presetComboBox;

    private File[] selectedFiles;
    private File saveFile;
    private List<UtilResult> results;

    final JFileChooser fc = new JFileChooser();
    final JFileChooser fcForResults = new JFileChooser();

    private DTCFilesListUtil dtcFilesListUtil;

    public FilesToolMainForm() {

        dtcFilesListUtil = new DTCFilesListUtil();

        selectFilesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!directoryField.getText().equals("")) {
                    fc.setCurrentDirectory(new File(directoryField.getText()));
                }

                fc.setMultiSelectionEnabled(true);
                fc.showDialog(null, "Select Files");
            }
        });

        fc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedFiles = fc.getSelectedFiles();
                displaySelectedFiles(selectedFiles);
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        analyzeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (selectedFiles == null || selectedFiles.length < 1) {
                    JOptionPane.showMessageDialog(null, "Please select files.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    directoryField.grabFocus();
                    return;
                }

                if (delimiterComboBox.getSelectedItem().toString().equals("")) {
                    JOptionPane.showMessageDialog(null, "Please select a delimiter.", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    delimiterComboBox.grabFocus();
                    return;
                }

                if (saveFile == null) {
                    JOptionPane.showMessageDialog(null, "Please select where to save the results file.",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    saveToButton.grabFocus();
                    return;
                }

                String delimiter = delimiterComboBox.getSelectedItem().toString();

                String[] ctrlColIndicesString = colsDupCheck.getText().split(",");
                String header = headerIdField.getText();
                results = TargetFilesUtil.processFiles(selectedFiles,
                        delimiter, ctrlColIndicesString, header);

                String resultPath = TargetFilesUtil.generateResultsFile(saveFile, results);
                JOptionPane.showMessageDialog(null, "Results file generated in: " + resultPath);

                clearFields();

            }
        });
        saveToButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int returnVal = fcForResults.showSaveDialog(null);
                if (returnVal == JFileChooser.CANCEL_OPTION) {
                    return;
                }
            }
        });

        fcForResults.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals("CancelSelection")) {
                    return;
                }
                saveFile = fcForResults.getSelectedFile();
                saveToLabel.setText(saveFile.getAbsoluteFile().toString());
            }
        });

        filesGenButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String outputDir = JOptionPane.showInputDialog("Enter CIM Output Directory", "\\\\uslv-papp-cims2\\CIM_Output");
                DTCFilesListUtil dtcFilesListUtil = new DTCFilesListUtil();
                if (!outputDir.isEmpty()) {
                    String notifMessage = dtcFilesListUtil.createMessage(outputDir);

                    JEditorPane textArea = new JEditorPane("text/html", notifMessage);
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    scrollPane.setPreferredSize(new Dimension(500, 500));
                    JOptionPane.showMessageDialog(null, scrollPane, "Production Notification Message",
                            JOptionPane.PLAIN_MESSAGE);

                }
            }
        });
        presetComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                clearFields();
                int selectedPreset = presetComboBox.getSelectedIndex() - 1;
                if (selectedPreset == DTCFilesListUtil.CATEGORY_DMC) {
                    // set presets for DMC
                    delimiterComboBox.setSelectedItem(",");
                    colsDupCheck.setText("4");
                    setSelectedFilesCategory(DTCFilesListUtil.CATEGORY_DMC);
                } else if (selectedPreset == DTCFilesListUtil.CATEGORY_SGI) {
                    // set presets for SHARPS
                    delimiterComboBox.setSelectedItem("|");
                    colsDupCheck.setText("1");
                    setSelectedFilesCategory(DTCFilesListUtil.CATEGORY_SGI);
                } else if (selectedPreset == DTCFilesListUtil.CATEGORY_SHARPS) {
                    // set presets for SHARPS
                    delimiterComboBox.setSelectedItem("|");
                    colsDupCheck.setText("1,7");
                    setSelectedFilesCategory(DTCFilesListUtil.CATEGORY_SHARPS);
                } else {
                    // clear presets
                    delimiterComboBox.setSelectedItem(null);
                    colsDupCheck.setText("");
                }
            }
        });
    }

    public void setSelectedFilesCategory(int category) {
        if (directoryField.getText() != null && !directoryField.getText().equals("")) {
            List<String> dirFilenames = dtcFilesListUtil.getFilenamesFromDir(directoryField.getText());
            List<String> catFilenames = dtcFilesListUtil.getCategoryFilenames(category, dirFilenames);
            selectedFiles = dtcFilesListUtil.getFilesFromFilenames(directoryField.getText(), catFilenames);
            displaySelectedFiles(selectedFiles);
        }
    }

    public void displaySelectedFiles(File[] selectedFiles) {
        if (selectedFiles.length > 0) {
            DefaultListModel model = new DefaultListModel();
            for (File file : selectedFiles) {
                model.addElement(file.getName());
            }
            selectedFilesDisplayList.setModel(model);
        }
    }

    public void clearFields() {
        if (selectedFiles != null) {
            Arrays.fill(selectedFiles, null);
            DefaultListModel listModel = (DefaultListModel) selectedFilesDisplayList.getModel();
            listModel.removeAllElements();
        }

        saveFile = null;
        saveToLabel.setText("");
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        JFrame frame = new JFrame("Amgen DTC Files Utility");
        frame.setContentPane(new FilesToolMainForm().mainPanel);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(9, 3, new Insets(15, 20, 15, 20), -1, -1));
        directoryPanel = new JPanel();
        directoryPanel.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(directoryPanel, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Directory:");
        directoryPanel.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        directoryField = new JTextField();
        directoryField.setText("\\\\uslv-papp-cims2\\CIM_Output");
        directoryPanel.add(directoryField, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        selectFilesButton = new JButton();
        selectFilesButton.setText("Select Files");
        directoryPanel.add(selectFilesButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        delimiterPanel = new JPanel();
        delimiterPanel.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(delimiterPanel, new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Delimiter:");
        delimiterPanel.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        delimiterComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("");
        defaultComboBoxModel1.addElement(",");
        defaultComboBoxModel1.addElement("|");
        defaultComboBoxModel1.addElement("t");
        delimiterComboBox.setModel(defaultComboBoxModel1);
        delimiterPanel.add(delimiterComboBox, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Columns for Duplicates Checking:");
        delimiterPanel.add(label3, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        colsDupCheck = new JTextField();
        colsDupCheck.setText("");
        colsDupCheck.setToolTipText("e.g. 1,2,3,4");
        delimiterPanel.add(colsDupCheck, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(buttonsPanel, new GridConstraints(8, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        analyzeButton = new JButton();
        analyzeButton.setText("Analyze");
        buttonsPanel.add(analyzeButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        buttonsPanel.add(spacer1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        cancelButton = new JButton();
        cancelButton.setText("Cancel");
        buttonsPanel.add(cancelButton, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        filesGenButton = new JButton();
        filesGenButton.setText("Create Notification");
        buttonsPanel.add(filesGenButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        listPanel = new JPanel();
        listPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(listPanel, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        listPanel.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(700, 300), null, 0, false));
        selectedFilesDisplayList = new JList();
        final DefaultListModel defaultListModel1 = new DefaultListModel();
        selectedFilesDisplayList.setModel(defaultListModel1);
        selectedFilesDisplayList.putClientProperty("List.isFileList", Boolean.TRUE);
        scrollPane1.setViewportView(selectedFilesDisplayList);
        headerDetailsPanel = new JPanel();
        headerDetailsPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(headerDetailsPanel, new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Header (column 1):");
        headerDetailsPanel.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        headerIdField = new JTextField();
        headerIdField.setEnabled(true);
        headerIdField.setText("user.Email");
        headerDetailsPanel.add(headerIdField, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(panel1, new GridConstraints(6, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        saveToButton = new JButton();
        saveToButton.setText("Save to");
        saveToButton.setMnemonic('S');
        saveToButton.setDisplayedMnemonicIndex(0);
        panel1.add(saveToButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        saveToLabel = new JLabel();
        saveToLabel.setText("");
        panel1.add(saveToLabel, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Results file:");
        panel1.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel1.add(spacer2, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(panel2, new GridConstraints(5, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JSeparator separator1 = new JSeparator();
        panel2.add(separator1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JSeparator separator2 = new JSeparator();
        mainPanel.add(separator2, new GridConstraints(7, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(panel3, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        panel3.add(spacer3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        presetComboBox = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        defaultComboBoxModel2.addElement("None");
        defaultComboBoxModel2.addElement("DMC");
        defaultComboBoxModel2.addElement("SGI");
        defaultComboBoxModel2.addElement("Sharps");
        presetComboBox.setModel(defaultComboBoxModel2);
        panel3.add(presetComboBox, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Preset:");
        panel3.add(label6, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Selected files:");
        label7.setToolTipText("");
        mainPanel.add(label7, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
