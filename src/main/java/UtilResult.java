/**
 * Created by jt186048 on 11/3/2015.
 */
public class UtilResult {

    private String filename;
    private String dateModified;
    private int lines;
    private DuplicateResult duplicates;

    public UtilResult(String filename, String dateModified, int lines, DuplicateResult duplicates) {
        this.setFilename(filename);
        this.setDateModified(dateModified);
        this.setLines(lines);
        this.setDuplicates(duplicates);
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public int getLines() {
        return lines;
    }

    public void setLines(int lines) {
        this.lines = lines;
    }

    public DuplicateResult getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(DuplicateResult duplicates) {
        this.duplicates = duplicates;
    }
}
