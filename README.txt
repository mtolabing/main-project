Amgen MS Files Utility README

=======================
System requirements
=======================
- Windows 7*
- Java Runtime Environment (1.6 up)

*not tested yet on any other OS, but may work

=======================
Running the app
=======================
1. Unzip the app, if you haven't already.
2. Open "out > artifacts > amgenfilesutil" and double-click "amgenfilesutil.jar"
3. Provide the ff:
	a. File Directory
		--must be absolute path, e.g. c:/folder 1/folder 2/folder 3
	b. Data Delimiter
		--select {,}, {|}, {t} for tabs, {s} for whitespace
	c. Columns to check for duplicates
		--you may indicate multiple column indices (count starts at 1),
		the utility will check combination of all columns for duplicates

		ex.
		Col1 | Col2 | Col3 | Col4
		a	b	c	d
		a	c	e	f
		a	b	c	d
		a	g	z	x

		-- utility will find 1 duplicate value for combination of "a b c d"	